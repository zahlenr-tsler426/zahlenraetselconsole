/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i3a.granoa.zahlenraetselconsole;

import java.util.ArrayList;
import org.junit.Test;

/**
 *
 * @author 1810g
 */
public class SpielTest {

	public SpielTest() {
	}

	@Test
	public void testShuffled() {
		Spiel s = new Spiel();
		int[] field = s.getFeld();
		boolean ordered = true;
		for (int i = 0; i < 15; i++) {
			if (field[i] + 1 != field[i + 1] && field[i] != 0 && field[i + 1] != 0) {
				ordered = false;
			}
		}
		assert (!ordered);

	}

	@Test
	public void adjacentTest() {
		Spiel s = new Spiel();
		assert (!s.isAdjacent(7, 13));
	}

	@Test
	public void contains0() {
		Spiel s = new Spiel();
		boolean contains = false;
		int[] checkArr = s.getFeld();
		for (int x : checkArr) {
			if (x == 0) {
				contains = true;
			}
		}
		assert (contains);
	}

	@Test
	public void test1_15() {
		Spiel s = new Spiel();
		ArrayList<Integer> allNums = new ArrayList();
		for (int i = 0; i < 15; i++) {
			allNums.add(i + 1);
		}
		int[] checkArr = s.getFeld();
		for (int x : checkArr) {
			if (x != 0) {
				allNums.remove(allNums.indexOf(x));
			}
		}
		assert (allNums.isEmpty());
	}

}
