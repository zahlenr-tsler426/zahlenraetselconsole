/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i3a.granoa.zahlenraetselconsole;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1810g
 */
public class Spiel {
	int[]feld=new int[16];

	public Spiel(){
		List<Integer>allNums=new ArrayList();
		for(int i=0;i<16;i++){
			allNums.add(i);
		}
		Collections.shuffle(allNums);

		for(int i=0;i<16;i++){
		feld[i]=allNums.get(i);
		}
	}

	public int[] getFeld() {
		return feld;
	}

	public void setFeld(int[] feld) {
		this.feld = feld;
	}
	
	public void play() {
		while (!isDone()) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			printBox();
			System.out.println("\n");
			try {
				String input = reader.readLine();
				int currentEmpty=0;
				for(int i=0;i<16;i++){
					if(feld[i]==0){
					currentEmpty=i;	
					}
				}
				int userBin=Integer.parseInt(input);
				int userIn=0;
				for(int i=0;i<16;i++){
					if(feld[i]==userBin){
					userIn=i;	
					}
				}
				
				if(userIn!=currentEmpty){
					if(isAdjacent(userIn,currentEmpty)){
						move(userIn,currentEmpty);
					}else{
						System.out.println("Invalide eingabe");
					}
				}
			}
			catch (IOException ex) {
				Logger.getLogger(Spiel.class.getName()).log(Level.SEVERE, null, ex);
			}

		}
		System.out.println("Gewonnen!!");
	}

	public boolean isDone() {
		for (int i = 0; i < 14; i++) {
			if (feld[i] + 1 != feld[i + 1]) {
				return false;
			}
		}

		return true;
	}

	public void printBox() {
		for (int i = 0; i < 4; i++) {
			System.out.println("\n");
			for (int j = 0; j < 4; j++) {
				System.out.print(feld[(4 * i) + j] + " ");
			}
		}
	}

	public boolean isAdjacent(int a, int b) {
		if ((a + 1) == b || a - 1 == b || a + 4 == b || a - 4 == b && (feld[a] == 0 || feld[b] == 0)) {
			return true;
		}
		return false;
	}

	public void move(int place, int destination) {
		if (feld[destination] == 0 && isAdjacent(place, destination)) {
			int temp = feld[place];
			feld[place] = feld[destination];
			feld[destination] = temp;

		}
	}



	
}
